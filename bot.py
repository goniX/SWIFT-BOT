import json
import discord
import asyncio
import os

client = discord.Client()
yes = {"Yes","Sure","yes"}
no = {"no","stop"}
quests = {}
for filename in os.listdir("Quests"):
    with open("Quests\\"+filename,"r") as readfile:
        quests[filename[:-5]] = json.load(readfile)

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

@client.event
async def on_message(message):
        global quests
        if message.content.startswith("!swift new quest"):
            print("making a fresh new quest")
            quest_client = message.author.name
            await client.send_message(message.channel,"A name for your quest")
            name = await client.wait_for_message(author=message.author)
            name = name.content
            if name+".json" in os.listdir("Quests"):
                await client.send_message(message.channel,"Sorry you can create a quest with this name")
                return
            await client.send_message(message.channel,"The task")
            task = await client.wait_for_message(author=message.author)
            task = task.content
            await client.send_message(message.channel,"The deadline")
            deadline = await client.wait_for_message(author=message.author)#
            deadline = deadline.content
            await client.send_message(message.channel,"The reward(s)")
            rewards = await client.wait_for_message(author=message.author)
            rewards = rewards.content.split(" ")
            await client.send_message(message.channel,"The difficulty")
            difficulty = await client.wait_for_message(author=message.author)
            difficulty = difficulty.content
            embed=discord.Embed(title=name, description=task, color=0x3366ff)
            embed.add_field(name="deadline",value=deadline)
            embed.add_field(name="rewards",value=" ".join(rewards))
            embed.add_field(name="difficulty",value=difficulty)
            embed.set_thumbnail(url=message.author.avatar_url)
            await client.send_message(message.channel,embed=embed)
            await client.send_message(message.channel,"Is this correct?")
            correct = await client.wait_for_message(author=message.author)
            if correct.content in yes:
                await client.send_message(message.channel,"Your quest has been added "+quest_client)
                quest = {"creator":quest_client,"name":name,"task":task,"time":deadline,"rewards":rewards,"difficulty":difficulty}
                quest_file = open("Quests\\"+name+".json","w")
                json.dump(quest,quest_file)
                quest_file.close()
                quests[name] = quest
            elif correct.content in no:
                await client.send_message(message.channel,"Your quest was not addedd")
            else:
                return
        elif message.content.startswith("!swift search quest"):
            await client.send_message(message.channel,"Filter by name,difficulty,creator or deadline?")
            filter_by = await client.wait_for_message(author=message.author)
            filter_by = filter_by.content.lower()
            await client.send_message(message.channel,"Search for?")
            filter_val = await client.wait_for_message(author=message.author)
            filter_val = filter_val.content
            fitting = []
            if filter_by == "name":
                for quest in quests:
                    print(filter_val)
                    if quests[quest]["name"] == filter_val:
                        fitting.append(quests[quest])
            elif filter_by == "difficulty":
                for quest in quests:
                    print(filter_val)
                    if quests[quest]["difficulty"] == filter_val:
                        fitting.append(quests[quest])
            elif filter_by == "creator":
                for quest in quests:
                    print(filter_val)
                    if quests[quest]["creator"] == filter_val:
                        fitting.append(quests[quest])
            elif filter_by == "deadline":
                for quest in quests:
                    print(filter_val)
                    if quests[quest]["deadline"] == filter_val:
                        fitting.append(quests[quest])
            if len(fitting) == 1:
                await client.send_message(message.channel,"Found 1 quest for ya")
            else:
                await client.send_message(message.channel,"Found "+str(len(fitting))+" quests for ya")
            await client.send_message(message.channel,"This is the first one")
            embed=discord.Embed(title=fitting[0]["name"], description=fitting[0]["task"], color=0x3366ff)
            embed.add_field(name="deadline",value=fitting[0]["time"])
            embed.add_field(name="rewards",value=" ".join(fitting[0]["rewards"]))
            embed.add_field(name="difficulty",value=fitting[0]["difficulty"])
            embed.set_thumbnail(url=message.author.avatar_url)
            await client.send_message(message.channel,embed=embed)

        elif message.content.startswith("!swift delete quest"):
            deleter = message.author.name
            deletable = []
            for quest in quests:
                if quests[quest]["creator"] == deleter:
                    deletable.append(quest)
            if len(deletable) == 0:
                await client.send_message(message.channel,"Found no quests made by you")
            elif len(deletable) == 1:
                await client.send_message(message.channel,"You only have the '"+quests[deletable[0]]["name"]+"' quest, delete it?")
                answer = await client.wait_for_message(author=message.author)
                answer = answer.content
                if answer in yes:
                    os.remove("Quests\\"+quests[deletable[0]]["name"]+".json")
                    await client.send_message(message.channel,"Deleted your quest "+deleter)
                elif answer in no:
                    await client.send_message(message.channel,"Your quest was not deleted")
            else:
                try:
                    final = "```\n"
                    counter = 1
                    for quest in deletable:
                        final+=str(counter)+") "+quest+"\n"
                        counter = counter+1
                    final += "```"
                    await client.send_message(message.channel,"Here are your quests\n"+final)
                except:
                    await client.send_message(message.channel,"Sorry you got too many quests, contact a bot dev for help pls")
                await client.send_message(message.channel,"Send the number of the quest to delete or no")
                number = await client.wait_for_message(author=message.author)
                number = number.content
                try:
                    number = int(number)-1
                except:
                    if number in no:
                        await client.send_message(message.channel,"Not deleting your quest")
                        return
                    else:
                        await client.send_message(message.channel,"Not a number sry")
                        return
                if number >= 0:
                    os.remove("Quests\\"+quests[deletable[number]]["name"]+".json")
                else:
                    await client.send_message(message.channel,"Your number is too small sorry")
                await client.send_message(message.channel,"Deleted your quest "+deleter)

            quests = {}
            for filename in os.listdir("Quests"):
                with open("Quests\\"+filename,"r") as readfile:
                    quests[filename[:-5]] = json.load(readfile)

        elif message.content.startswith("!swift add idea"):
            await client.send_message(message.channel,"Send a name for you bot command idea")
            ideaname = await client.wait_for_message(author=message.author)
            ideaname = ideaname.content
            if ideaname not in os.listdir("Idea"):
                await client.send_message(message.channel,"What is your idea?")
                idea = await client.wait_for_message(author=message.author)
                idea = idea.content
                with open("Idea\\"+ideaname+".txt","w") as ideafile:
                    ideafile.write(idea)
                await client.send_message(message.channel,"Your idea was added")
            else:
                await client.send_message(message.channel,"An with that name already exists sry")

        elif message.content.startswith("!swift help"):
            helpmsg = "```\n"
            helpmsg+="!swift create quest for making a new quest\n"
            helpmsg+="!swift search quest for searching quests\n"
            helpmsg+="!swift delete quest for deleting a quest\n"
            helpmsg+="!swift add idea for adding a new bot command idea\n"
            
            helpmsg+="```"
            await client.send_message(message.channel,helpmsg)
print()
client.run(json.load(open("secret.json","r"))["api_key"])
